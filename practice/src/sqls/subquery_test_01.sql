/*
 * 请告诉我那些没有下任何订单的顾客（customer）的信息。结果应当包含如下的信息：
 *
 * +─────────────────+───────────────+
 * | customerNumber  | customerName  |
 * +─────────────────+───────────────+
 *
 * 结果应当按照 `customerNumber` 排序。
 */
 select cus.customerNumber,cus.customerName from
 customers cus left join orders ord
 on cus.customerNumber=ord.customerNumber
 where isnull(ord.customerNumber)
