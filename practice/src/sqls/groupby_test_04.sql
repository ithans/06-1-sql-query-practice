/*
 * 请告诉我总金额大于 60000 的每一个订单（`order`）编号及其总金额。查询结果应当包含如下信息：
 * 
 * +──────────────+─────────────+
 * | orderNumber  | totalPrice  |
 * +──────────────+─────────────+
 *
 * 其结果应当以 `orderNumber` 排序。
 */
 select ord.orderNumber,sum(priceEach*quantityOrdered)as totalPrice from
 orders ord left join orderdetails details
 on ord.orderNumber=details.orderNumber
 group by ord.orderNumber
 having sum(priceEach*quantityOrdered)>60000